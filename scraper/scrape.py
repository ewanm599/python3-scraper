import html.parser
import os
import sys
import urllib.request
from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup

def simple_get(url):
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return none
    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return none
    
def is_good_response(resp):
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 and content_type is not None and content_type.find('html') > -1)

def rawhtml():
    url = 'https://' + str(str(sys.argv[1]))
    raw_html = simple_get(url)
    html = BeautifulSoup(raw_html,'html.parser')
    return(html,url)

def download_image():
    html = rawhtml()
    for image in html[0].findAll('a', {"href":True}):
        href = image['href']
        if ".png" in href or ".jpg" in href or ".webm" in href or ".gif" in href:
            try:
            
                x = sys.argv[1]
                src = image['href']
                loc = 'https:'+src
                file_name = src.split('/')[-1]
                cur_path = "/home/"+os.environ["USER"]+"/"
                if not os.path.exists(os.path.join(cur_path, 'images/'+x[-7:]+"/")):
                    os.makedirs(os.path.join(cur_path, 'images/'+x[-7:]+"/"))
                f_path = os.path.join(cur_path, 'images/'+x[-7:]+"/"+ file_name)
                urllib.request.urlretrieve(loc, f_path)
                print('Download Complete')
            except:
                print('ERROR: file name is incorrect : ','https:'+image['href'])
    print('No more images on this page')
download_image()
